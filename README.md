Inspired by [this repo](https://github.com/medivo/ansible_playbooks) and
[this](http://www.medivo.com/blog/rapidly-provisioning-machines/) article.

# Vagrant + Ansible = 🎉

## Getting up and running

Install Vagrant and Ansible, then do `vagrant up`. To re-provision the virtual
machine (e.g. after changing the playbook), do `vagrant provision`. This sets up
a VM with Redis, NodeJS, Rails (pre-release), Ruby 2.3.0 and an example Rails
app (contained in `rails_example_app`).

Note: Currently does not update any packages in LTS (probably insecure), as this
takes quite some time. But this should be easy to do using Ansible.

TODO: Currently need to start Rails manually. Due to Rails binding to 127.0.0.1,
need to do `rails s -b 0.0.0.0` to allow for port forwarding in Vagrant.

TODO: Make the Rails app use the Postgres DB.

## Literature

- [Ansible: Best Practices](https://docs.ansible.com/ansible/playbooks_best_practices.html#directory-layout).
  We are probably breaking a couple of these right now.
- [Learning Ansible with Vagrant (2/4)](https://sysadmincasts.com/episodes/45-learning-ansible-with-vagrant-part-2-4). Great series on DevOps with Ansible.
